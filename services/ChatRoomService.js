angular.module('mychatsample')
    .factory('ChatRoomService', function () {

        var socket = io.connect(window.location.hostname);

        return {
            newChatRoom: function (chatRoomName) {
                socket.emit('newChatRoom', chatRoomName);
            },
            connectChatRoom: function (chatRoomName, pseudo) {
                socket.emit('connectChatRoom', chatRoomName, pseudo);
            },
            leaveChatRoom: function (chatRoomName, pseudo) {
                socket.emit('leaveChatRoom', chatRoomName, pseudo);
            }
        }
    })

;