angular.module('mychatsample')
    .factory('UserService', function ($rootScope) {

        var socket = io.connect(window.location.hostname);
        var listUsers = [];

        socket.on('listusersinchatroom', function (users) {
            listUsers = users;
            $rootScope.$broadcast('pseudos', users);
        });

        return {
            newUser: function (pseudo) {
                socket.emit('nouveau_client', pseudo);
            },

            getPseudos: function () {
                return pseudos;
            }
        }
    })

;