angular.module('mychatsample').
    factory('myChatFactory', function ($rootScope) {

        var socket = io.connect(window.location.hostname);
        var listUsers = [];

        socket.on('chatrooms', function (chatrooms) {
            $rootScope.$broadcast('chatrooms', chatrooms);
        });


        return {
            getUsers: function () {
                return listUsers;
            },

            send: function (chatRoomName) {
                socket.emit('getUsers', chatRoomName);
            },
            sendChatRooms: function () {
                socket.emit('getChatRooms');
            }
        }
    }
)
    .factory('UserConnectionFactory', function ($rootScope) {

        var connected = false;
        var user = '';
        var chatRoom = '';

        return {
            setConnected: function (isConnected) {
                connected = isConnected;
                $rootScope.$broadcast('connected', isConnected);
            },
            getConnected: function () {
                return connected;
            },
            setUser: function (pseudo) {
                user = pseudo;
            },
            getUser: function () {
                return user;
            },
            setChatRoom: function (chatRoomName) {
                chatRoom = chatRoomName;
            },
            getChatRoom: function () {
                return chatRoom;
            }
        }
    })
;
