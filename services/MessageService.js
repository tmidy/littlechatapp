angular.module('mychatsample')
    .factory('MessageService', function () {
        // Connexion à socket.io
        var socket = io.connect(window.location.hostname);
        var pseudoCurrent = '';
        var chatRoomName = '';

        // Quand on reçoit un message, on l'insère dans la page
        socket.on('message', function (data) {
            if (chatRoomName != '' && data.chatRoom== chatRoomName) {
                $("#zone_chat").val($("#zone_chat").val() + '\n' + data.pseudo + ' : ' + data.message);
            }
        })

        // Quand un nouveau client se connecte, on affiche l'information
        socket.on('nouveau_client', function (docs) {
            if (chatRoomName != '' && docs.chatRoom == chatRoomName) {
                $("#zone_chat").val($("#zone_chat").val() + '\n' + docs.pseudo + '  a rejoint le Chat ! ');
            }
        })

        // Quand un nouveau client se connecte, on affiche l'information
        socket.on('logoutUser', function (docs) {
            if (chatRoomName != '' && docs.chatRoomName == chatRoomName) {
                $("#zone_chat").val($("#zone_chat").val() + '\n' + docs.pseudo + '  a quitté le Chat ! ');
            }
        })


        return {
            newUser: function (pseudo) {
                pseudoCurrent = pseudo;
                socket.emit('nouveau_client', pseudo);
            },

            // Ajoute un message dans la page
            insereMessage: function (pseudo, message, chatRoom) {
                if (pseudo != null && pseudo != '') {
                    socket.emit('message', message, chatRoom);
                    $("#zone_chat").val($("#zone_chat").val() + '\n' + pseudo + ' : ' + message);
                }
            },

            logoutUser: function (pseudo, chatRoom) {
                if (pseudo != null && pseudo != '') {
                    socket.emit('logout', pseudo, chatRoom);
                }
            },

            setChatRoom: function (chatRoom) {
                chatRoomName = chatRoom;
            },

            ////returns the user pseudo
            getPseudo: function () {
                return pseudoCurrent;
            }
        }
    })
;