/*BDD Treatments*/
var mongoose = require('mongoose');

var connection = '';
var res = [];
var resChat = [];

module.exports.connect = function () {
    if (connection == '') {
        connection = mongoose.createConnection('mongodb://tangi:tangi@ds041164.mongolab.com:41164/heroku_8dwd32bm', function (err) {
            if (err) {
				console.log(err);
                throw err;
            }
        });
    }
}
;

module.exports.createEntities = function () {

    // MESSAGES
    var messages = new mongoose.Schema({
        user_id: {type: String},
        contenu: String,
        date: {type: Date, default: Date.now}
    });
    var messagesModel = mongoose.model('messages', messages);

    // USERS
    var chatusers = new mongoose.Schema({
        pseudo: {type: String},
        chatRoom_id: {type: String, required: false}
    });
    var usersModel = mongoose.model('chatusers', chatusers);


    //CHAT ROOMS
    var chatroom = new mongoose.Schema({
        name: {type: String, match: /[a-zA-Z0-9-_]+$/},
        date: {type: Date, default: Date.now}
    });
    var chatRoomModel = mongoose.model('chatroom', chatroom);
}

module.exports.closeConnection = function () {
    mongoose.connection.close();
}

module.exports.log = function (message) {
    console.log(message);
};

module.exports.addMessage = function (user, message) {
    var messagesModel = connection.model('messages');
    var message = new messagesModel({user_id: user, contenu: message});
    message.save(function (err) {
        if (err) {
            throw err;
        }
    });
};

// USERS
module.exports.addChatUser = function (pseudo) {
    var usersModel = connection.model('chatusers');
    var user = new usersModel({pseudo: pseudo});
    user.save(function (err) {
        if (err) {
            throw err;
        }

        return true;
    });
};
module.exports.logoutUser = function (pseudo) {
    var usersModel = connection.model('chatusers');
    usersModel.remove({pseudo: pseudo}, function (err) {
        if (err) {
            console.log(err)
        } else {
            return 'User ' + pseudo + ' successfully removed';
        }
    });
};

module.exports.getAllUsers = function () {
    var usersModel = connection.model('chatusers');
    var query = usersModel.find({}, {'pseudo': 1, '_id': 0, 'chatRoom_id': 1});
    return query;
};


module.exports.getUsersByChatRoom = function (chatRoomName) {
    var usersModel = connection.model('chatusers');
    var query = usersModel.find({'chatRoom_id': chatRoomName}, {'pseudo': 1, '_id': 0});
    return query;
};

module.exports.connectChatRoom = function (chatRoomName, pseudo) {
    var usersModel = connection.model('chatusers');
    usersModel.update({pseudo:pseudo}, {chatRoom_id:chatRoomName} ,  function (err) {
        if (err) {
            console.log(err)
        } else {
            return 'User ' + pseudo + ' successfully removed';
        }
    });
};

module.exports.logoutChatRoom = function (pseudo) {
    var usersModel = connection.model('chatusers');
    usersModel.update({pseudo:pseudo}, {chatRoom_id:''} ,  function (err) {
        if (err) {
            console.log(err)
        } else {
            return 'User ' + pseudo + ' successfully removed from chatroom';
        }
    });
};


// CHATROOMS
module.exports.getAllChatRooms = function () {
    var chatRoomModel = connection.model('chatroom');
    var query = chatRoomModel.find({}, {'name': 1, '_id': 0});

    return query;
};

module.exports.addChatRoom = function (name) {
    var chatRoomModel = connection.model('chatroom');
    var chatRoom = new chatRoomModel({name: name});
    chatRoom.save(function (err) {
        if (err) {
            throw err;
        }

        return true;
    });
};


module.exports.getResChat = function () {
    return resChat;
};

