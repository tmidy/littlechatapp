module.exports.executeQueryAndBroadcast = function (query, broadcastMessage, socket, chatRoomName) {
    query.exec(function (err, docs) {
        socket.emit(broadcastMessage, {docs: docs, chatRoomName: chatRoomName});
        socket.broadcast.emit(broadcastMessage, {docs: docs, chatRoomName: chatRoomName});
    });
};

