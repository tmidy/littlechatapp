'use strict';
angular.module('mychatsample')
    .controller('MyChatController', function ($scope, UserConnectionFactory) {

        $scope.isConnected=false;

        $scope.$on('connected', function () {
            $scope.isConnected = UserConnectionFactory.getConnected();
        })
    });
