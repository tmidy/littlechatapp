'use strict';
angular.module('mychatsample')
    .controller('ChatRoomsController', function ($scope, $rootScope, MessageService, myChatFactory, ChatRoomService, UserConnectionFactory, $state, $stateParams) {


        // if not connected
        if (UserConnectionFactory.getConnected() == false) {
            $state.go('home');
        }

        $scope.chatRoomName = '';
        if ($stateParams.chatRoomName) {
            $scope.chatRoomName = $stateParams.chatRoomName;
        }

        $scope.chatRoomId = '';

        // Refresh available chat rooms
        myChatFactory.sendChatRooms();

        $scope.$on('chatrooms', function (event, chatrooms) {
            console.log(chatrooms);
            $scope.chatRoomsList = chatrooms.docs;
            $scope.$apply();
        });

        $scope.createChatRoom = function () {
            ChatRoomService.newChatRoom($scope.chatRoomName);
            $state.go('chatrooms');
        }

        $scope.connectChatRoom = function (chatRoomName) {
            UserConnectionFactory.setChatRoom(chatRoomName);
            MessageService.setChatRoom(chatRoomName);
            ChatRoomService.connectChatRoom(chatRoomName, UserConnectionFactory.getUser());
        }

    });
