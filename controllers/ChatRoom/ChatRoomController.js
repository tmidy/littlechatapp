'use strict';
angular.module('mychatsample')
    .controller('ChatRoomController', function ($scope, $rootScope, MessageService, myChatFactory, ChatRoomService, UserService, UserConnectionFactory, $state, $stateParams) {


        // if not connected
        if (UserConnectionFactory.getConnected() == false) {
            $state.go('home');
        }

        $scope.messages = [];
        $scope.message = '';
        $scope.chatRoomName = '';
        if ($stateParams.chatRoomName) {
            $scope.chatRoomName = $stateParams.chatRoomName;
        }
        // send list of users in the chat room
        myChatFactory.send($scope.chatRoomName);

        $scope.$on('pseudos', function (event, pseudos) {
            if ($scope.chatRoomName == pseudos.chatRoomName) {
                $scope.connectedUser = pseudos.docs;
                $scope.$apply();
            }
        });

        $scope.sendMessage = function () {
            MessageService.insereMessage(MessageService.getPseudo(), $scope.message, $scope.chatRoomName);
            $scope.messages.push($scope.message);
            $scope.message = '';
        }

        $scope.leaveChatRoom = function () {
            ChatRoomService.leaveChatRoom($scope.chatRoomName, MessageService.getPseudo());
        }
    });
