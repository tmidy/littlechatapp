'use strict';
angular.module('mychatsample')
    .controller('StorageController', function ($scope,
                                  $localStorage,
                                  $sessionStorage) {

        $scope.$storage = $localStorage;

        $scope.save = function(message) {
            $localStorage.message = message;
        }

        $scope.load = function() {
            $scope.data = $localStorage.message;
        }

    });