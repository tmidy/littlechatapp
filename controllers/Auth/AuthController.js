'use strict';
angular.module('mychatsample')
    .controller('AuthController', function ($scope, $state, MessageService, UserConnectionFactory, $localStorage) {

        $scope.pseudo = '';

        $scope.login = function () {
            MessageService.newUser($scope.pseudo);
            //Go to the chatroom after login
            $state.go('chatrooms');
            UserConnectionFactory.setConnected(true);
            UserConnectionFactory.setUser($scope.pseudo);
            $localStorage.pseudo = $scope.pseudo;
        }

        $scope.logout = function () {
            MessageService.logoutUser($localStorage.pseudo, UserConnectionFactory.getChatRoom());
            $state.go('home');
            UserConnectionFactory.setConnected(false);
        }
    });
