// Make sure to include the `ui.router` module as a dependency
angular.module('mychatsample', ['ui.router', 'ngStorage'])

    .run(
    ['$rootScope', '$state', '$stateParams',
        function ($rootScope, $state, $stateParams) {

            // It's very handy to add references to $state and $stateParams to the $rootScope
            // so that you can access them from any scope within your applications.For example,
            // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
            // to active whenever 'contacts.list' or one of its decendents is active.
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }
    ]
)
    .config(
    ['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            /////////////////////////////
            // Redirects and Otherwise //
            /////////////////////////////

            // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
            $urlRouterProvider

                // The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
                // Here we are just setting up some convenience urls.
                // If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
                .otherwise('/');


            //////////////////////////
            // State Configurations //
            //////////////////////////

            // Use $stateProvider to configure your states.
            $stateProvider
                .state("home", {
                    url: "/",
                    templateUrl: './views/home.html'

                })
                .state('auth', {
                    url: '/auth',

                    templateUrl: './views/Auth/auth.html'
                })
                .state('chatrooms', {
                    url: '/chatrooms',
                    templateUrl: './views/chat/chatrooms.html'
                })
                .state('chatroom', {
                    url: '/chatroom/:chatRoomName',
                    templateUrl: './views/chat/chatroom.html',
                    params: {
                        chatRoomName: null
                    }
                })
                .state('createCR', {
                    url: '/chatrooms/create',
                    templateUrl:'./views/chat/createCR.html'
                })
        }
    ]
);
