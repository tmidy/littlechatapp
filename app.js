﻿var app = require('express')(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    ent = require('ent'), // Permet de bloquer les caractères HTML (sécurité équivalente à htmlentities en PHP)
    fs = require('fs'),
    path = require('path');

var Entities = require('html-entities').XmlEntities;
entities = new Entities();

var express = require('express');

//External Utils
var mongoTreatment = require('./mongo/mongoTreatment');
var broadcastMongo = require('./mongo/broadcastMongoUtils');
// End External Utils

//Set default path to .
app.use(express.static(__dirname));

// Set index.html as default page
app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});


// sockets events
io.sockets.on('connection', function (socket) {
    // Dès qu'on nous donne un pseudo, on le stocke en variable de session et on informe les autres personnes
    socket.on('nouveau_client', function (pseudo) {

        pseudo = ent.encode(pseudo);
        socket.pseudo = entities.decode(pseudo);

        mongoTreatment.connect();
        mongoTreatment.addChatUser(entities.decode(pseudo));
        mongoTreatment.closeConnection();
        mongoTreatment.connect();
        mongoTreatment.closeConnection();
    });
    //MESSAGES
    // Dès qu'on reçoit un message, on récupère le pseudo de son auteur et on le transmet aux autres personnes
    socket.on('message', function (message, chatRoom) {
        message = ent.encode(message);
        mongoTreatment.connect();
        mongoTreatment.addMessage(socket.pseudo, entities.decode(message));
        mongoTreatment.closeConnection();
        socket.broadcast.emit('message', {pseudo: socket.pseudo, message: entities.decode(message), chatRoom: chatRoom});
    });

    //USERS
    socket.on('logout', function (pseudo, chatRoomName) {
        mongoTreatment.connect();
        var result = mongoTreatment.logoutUser(pseudo);

        socket.broadcast.emit('logoutUser', {pseudo: pseudo, chatRoomName: chatRoomName});
        var query = mongoTreatment.getUsersByChatRoom(chatRoomName);
        broadcastMongo.executeQueryAndBroadcast(query, 'listusersinchatroom', socket, chatRoomName);
        mongoTreatment.closeConnection();
    });

    socket.on('leaveChatRoom', function (pseudo, chatRoomName) {
        mongoTreatment.connect();
        var result = mongoTreatment.logoutChatRoom(pseudo);

        socket.broadcast.emit('logoutUser', {pseudo: pseudo, chatRoomName: chatRoomName});
        var query = mongoTreatment.getUsersByChatRoom(chatRoomName);
        broadcastMongo.executeQueryAndBroadcast(query, 'listusersinchatroom', socket, chatRoomName);
        mongoTreatment.closeConnection();
    });

    socket.on('getUsers', function (chatRoomName) {
        mongoTreatment.connect();
        var query = mongoTreatment.getUsersByChatRoom(chatRoomName);
        broadcastMongo.executeQueryAndBroadcast(query, 'listusersinchatroom', socket, chatRoomName);
        mongoTreatment.closeConnection();
    });


    //CHATROOMS
    socket.on('getChatRooms', function () {
        mongoTreatment.connect();
        var query = mongoTreatment.getAllChatRooms();
        broadcastMongo.executeQueryAndBroadcast(query, 'chatrooms', socket);
        mongoTreatment.closeConnection();
    });

    socket.on('newChatRoom', function (chatRoomName) {
        mongoTreatment.connect();
        mongoTreatment.addChatRoom(chatRoomName);
        var query = mongoTreatment.getAllChatRooms();
        broadcastMongo.executeQueryAndBroadcast(query, 'chatrooms', socket);
        mongoTreatment.closeConnection();
    });
    socket.on('connectChatRoom', function (chatRoomName, pseudo) {
        mongoTreatment.connect();
        mongoTreatment.connectChatRoom(chatRoomName, pseudo);
        var query = mongoTreatment.getUsersByChatRoom(chatRoomName);
        broadcastMongo.executeQueryAndBroadcast(query, 'listusersinchatroom', socket, chatRoomName);
        mongoTreatment.closeConnection();
        socket.broadcast.emit('nouveau_client', {pseudo:entities.decode(pseudo), chatRoom:chatRoomName});
    });
});


mongoTreatment.connect();
mongoTreatment.createEntities();

// Server listen
server.listen(process.env.PORT || 5000);




